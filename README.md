# sw15_eit_interpolation

SW15 - interpolation strategies for missing voltage data in Electrical Impedance Tomography (EIT) - Ahmed Zacky M.Y., Kai Anlauf, under Jöran Rixen.  
Git repo mainly for archiving right now.  
 
# What is where?

docu/ contains documentation and relevent presentation files that give a high-level introduction from our perspective to the project.  
files/ contains the files necessary to run the software solution. Make sure to read the documentation first as it has the details about these files.   
img/ has some images and snapshots (some are well labeled) from during my time with the project. There are some relevant insights in there that can save future users and devs from pitfalls surrounding this project.  
