numSets = 1;  % Number of sets

% Create the HDF5 file
file = H5F.create('d345378545ta2.h5');

% Create the dataset group

plist = 'H5P_DEFAULT';
group = H5G.create(file, '/dataset_group',plist,plist,plist);
%group_id = H5G.create(file, '/dataset_group');
%H5G.close(group_id);


for index = 1:numSets %run for amount of files needed

%% init
    % Load EIDORS
    run startup.m
    
    %make meshes
    mesh_13 = mk_common_model('j2C', 16); % finer circular mesh
    mesh_16 = mk_common_model('j2C', 16);
    
    %create sim img objects
    sim_img_13 = mk_image(mesh_13,1);
    sim_img_16 = mk_image(mesh_16,1);
    
    %define and assign stim patterns
    stim =  mk_stim_patterns(16,1,'{ad}','{ad}',{'no_meas_current'},1);
    sim_img_13.fwd_model.stimulation = stim;
    
    stim2 = mk_stim_patterns(16,1,'{ad}','{ad}',{'meas_current'},1);
    sim_img_16.fwd_model.stimulation = stim2;

%% random vals // uniformly distributed

    R = 1; % radius of FEM model
    x0 = 0; % Center of the circle in the x direction.
    y0 = 0; % Center of the circle in the y direction.
    t = 2*pi*rand();
    r = R*rand();

    xr = x0 + r.*cos(t);
    yr = y0 + r.*sin(t);
    
    selectionRadius = rand(); % 0-1
    selectionScalar = rand(); % 0-1

 %% selection
    select_fcn =  @(x,y,z) (x-xr).^2+(y-yr).^2<(selectionRadius^2);


    sim_img_13.elem_data = selectionScalar + elem_select(sim_img_13.fwd_model,select_fcn);
    sim_img_16.elem_data = selectionScalar + elem_select(sim_img_16.fwd_model,select_fcn);

   
    diagVals = sprintf('Diag: Center = %f,%f , Radius = %f ,SelectionScalar = %f',xr,yr,selectionRadius,selectionScalar);
    disp (diagVals);

 %% 
    
    hmg_data_13=fwd_solve(sim_img_13);
    hmg_data_16=fwd_solve(sim_img_16);

    meas1 = hmg_data_13.meas;
    meas2 = hmg_data_16.meas;


    %% plot

     figure;
     show_fem(sim_img_13);
% 
%     figure;
%     show_fem(sim_img_16);

%% HDF5 STUFF

    vector_13_data = meas1;  % Replace with your actual data
    vector_13_name = ['/dataset_group/vector_13_', num2str(index)];
    h5create('data2.h5', vector_13_name, size(vector_13_data));
    h5write('data2.h5', vector_13_name, vector_13_data);

    % Create the vector_16 dataset
    vector_16_data = meas2;  % Replace with your actual data
    vector_16_name = ['/dataset_group/vector_16_', num2str(index)];
    h5create('data2.h5', vector_16_name, size(vector_16_data));
    h5write('data2.h5', vector_16_name, vector_16_data);


clear;
end


% Close the HDF5 file
% H5F.close('data.h5');